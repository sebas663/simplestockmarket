package simple.stock.market;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import simple.stock.market.entities.Stock;
import simple.stock.market.entities.Trade;
import simple.stock.market.factories.AbstractFactory;
import simple.stock.market.factories.FactoryProducer;
import simple.stock.market.services.MasterStocks;
import simple.stock.market.services.RecordStockTrade;
import simple.stock.market.services.StockIndicator;

public class SimpleStockMarketMain {
	
	private static Scanner in = new Scanner(System.in);
	private static AbstractFactory factory;
	private static MasterStocks masterStocks;
	private static StockIndicator stockIndicator; 
	private static RecordStockTrade recordStockTrade; 
	
	public static void main(String[] args) {

		loadMasterStocks();

		factory = FactoryProducer.getFactory("StockIndicator");
		stockIndicator = factory.getStockIndicator("StockIndicatorImpl");

		factory = FactoryProducer.getFactory("RecordStockTrade");
		recordStockTrade = factory.getRecordStockTrade("RecordStockTradeImpl");

		boolean getOut = false;
		int option = 0;
		do {
			System.out.println("Choose a menu option: ");
			System.out.println("Option 1 to enter a price and calculate the dividend yield of a stock.");
			System.out.println("Option 2 to enter a price and calculate the P/E ratio of a stock.");
			System.out.println("Option 3 to record a trade.");
			System.out.println("Option 4 to calculate Volume Weighted Stock Price based on trades in past  5 minutes.");
			System.out.println("Option 5 to calculate the GBCE All Share Index.");
			System.out.println("Option 0 to finish the program.");
			try {
				option = in.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("You entered an unexpected option. ");
				option = -1;
				in.nextLine();
			}
			System.out.println();
			switch (option) {
			case 1:
				calculateDividend();
				break;
			case 2:
				calculatePERatio();
				break;
			case 3:
				recordTrade();
				break;
			case 4:
				calculateVolumeWeightedStockPrice();
				break;
			case 5:
				calculateGBCEAllShareIndex();
				break;
			case 0:
				getOut = true;
				break;
			default:
				System.out.println("Invalid option --> " + option);
				System.out.println();
				break;
			}
			if (option == -1) {
				if (!askNewIncome("option")) {
					getOut = true;
				}
			}
		} while (!getOut);

		System.out.println("Finished program.");
	}
	
	/**
	 * 
	 */
	private static void calculateVolumeWeightedStockPrice() {
		Stock stock = getStock();
		if (stock != null) {
			List<Trade> trades = recordStockTrade.getLastFiveMinutesTradesByStockSymbol(stock.getStockSymbol());
			Double d = stockIndicator.calculateVolumeWeightedStockPrice(trades);
			System.out.println("The Volume Weighted Stock Price based on trades in past  5 minute is: " + d);
			System.out.println();
		}
	}
	
	/**
	 * 
	 */
	private static void calculateGBCEAllShareIndex() {
		List<Double> volumeWeightedStockPrices = new ArrayList<>();
		for (Stock stock : masterStocks.getlStoks()) {
			List<Trade> trades = recordStockTrade.getTradesByStockSymbol(stock.getStockSymbol());
			Double d = stockIndicator.calculateVolumeWeightedStockPrice(trades);
			volumeWeightedStockPrices.add(d);
		}
		Double d = stockIndicator.calculateGBCEAllShareIndex(volumeWeightedStockPrices);
		System.out.println("The  GBCE All Share Index is: " + d);
		System.out.println();
	}
	
	/**
	 * 
	 */
	private static void calculateDividend() {
		Stock stock = getStock();
		Double price = 0D;
		if (stock != null) {
			System.out.println("Enter the stock price: ");
			try {
				price = in.nextDouble();
				Double d = stockIndicator.calculateDividendYield(price, stock);
				System.out.println("The dividend yield is: " + d);
				System.out.println();
			} catch (InputMismatchException e) {
				System.out.println("You entered an unexpected option. ");
				in.nextLine();
			}

		}
	}
	
	/**
	 * 
	 */
	private static void calculatePERatio() {
		Stock stock = getStock();
		Double price = 0D;
		if (stock != null) {
			System.out.println("Enter the stock price: ");
			try {
				price = in.nextDouble();
				Double d = stockIndicator.calculatePERatio(price, stock);
				System.out.println("The  P/E ratio is: " + d);
				System.out.println();
			} catch (InputMismatchException e) {
				System.out.println("You entered an unexpected option. ");
				in.nextLine();
			}
		}
	}
	
	/**
	 * 
	 */
	private static void recordTrade() {
		boolean getOutOrder = false;
		boolean buyIndicator = false;
		boolean sellIndicator = false;
		String orderIndicator = "";
		Stock stock = getStock();
		String orderType = "";
		if (stock != null) {
			do {
				System.out.println("Enter the type of order 'b' to buy 's' to sell: ");
				orderType = in.next();
				switch (orderType) {
				case "b":
					buyIndicator = true;
					orderIndicator = "buy";
					break;
				case "s":
					sellIndicator = true;
					orderIndicator = "sell";
					break;
				default:
					System.out.println("Invalid option --> " + orderType);
					System.out.println();
					if (!askNewIncome("type of order")) {
						getOutOrder = true;
					}
				}
				if (!getOutOrder) {
					System.out.println("Enter the order quantity: ");
					try {
						Integer quantity = in.nextInt();
						if (quantity.intValue() > 0) {
							System.out.println("Enter the order price: ");
							Double price = in.nextDouble();
							if (price.doubleValue() > 0) {
								Trade trade = new Trade(stock.getStockSymbol(), LocalDateTime.now(), quantity,
										buyIndicator, sellIndicator, price);
								recordStockTrade.add(trade);
								getOutOrder = true;
								System.out.println("The order was entered is" + orderIndicator + " " + trade.toString());
								System.out.println();
							} else {
								System.out.println("Must enter a number greater than zero.");
								System.out.println();
							}
						} else {
							System.out.println("Must enter a number greater than zero.");
							System.out.println();
						}
					} catch (InputMismatchException e) {
						System.out.println("You entered an unexpected option. ");
						in.nextLine();
					}

				}

			} while (!getOutOrder);
		}
	}
	
	private static boolean askNewIncome(String income) {
		boolean newIncome = false;
		System.out.println("Do you want to enter another " + income + "? enter 'y' for new entry, any key to exit");
		String entry = in.next();
		System.out.println();
		if (entry.equals("y")) {
			newIncome = true;
		}
		return newIncome;
	}
	
	private static Stock getStock() {
		boolean getOut = false;
		Stock stock = null;
		String symbol = "";
		do {
			System.out.println("Enter the stock symbol: ");
			symbol = in.next();
			stock = masterStocks.getStockBySymbol(symbol);
			if (stock == null) {
				System.out.println("There is no stock with the symbol entered.");
				if (!askNewIncome("symbol")) {
					getOut = true;
				}
				System.out.println();
			} else {
				getOut = true;
			}

		} while (!getOut);
		return stock;
	}
	
	private static void loadMasterStocks() {
		
		factory = FactoryProducer.getFactory("MasterStocks");
		masterStocks = factory.getMasterStocks("MasterStocksImpl");

		Stock stock = new Stock("TEA", "Common", 0D, null, 100D);
		masterStocks.add(stock);
		stock = new Stock("POP", "Common", 8D, null, 100D);
		masterStocks.add(stock);
		stock = new Stock("ALE", "Common", 23D, null, 60D);
		masterStocks.add(stock);
		stock = new Stock("GIN", "Preferred", 8D, 2D, 100D);
		masterStocks.add(stock);
		stock = new Stock("JOE", "Common", 13D, null, 250D);
		masterStocks.add(stock);
	}

}
