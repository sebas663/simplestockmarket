package simple.stock.market.factories;

import simple.stock.market.services.MasterStocks;
import simple.stock.market.services.RecordStockTrade;
import simple.stock.market.services.StockIndicator;

public abstract class AbstractFactory {
	/**
	 * 
	 * @param operation
	 * @return
	 */
	public abstract StockIndicator getStockIndicator(String operation);

	/**
	 * 
	 * @param operation
	 * @return
	 */
	public abstract MasterStocks getMasterStocks(String operation);

	/**
	 * 
	 * @param operation
	 * @return
	 */
	public abstract RecordStockTrade getRecordStockTrade(String operation);
}
