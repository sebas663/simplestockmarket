package simple.stock.market.factories;

import simple.stock.market.services.MasterStocks;
import simple.stock.market.services.RecordStockTrade;
import simple.stock.market.services.StockIndicator;
import simple.stock.market.services.impl.StockIndicatorImpl;

public class StockIndicatorFactory extends AbstractFactory {
	
	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.AbstractFactory#getStockOperation(java.lang.String)
	 */
	@Override
	public StockIndicator getStockIndicator(String operation) {
		if (operation == null) {
			return null;
		}
		if (operation.equalsIgnoreCase("StockIndicatorImpl")) {
			return new StockIndicatorImpl();

		}
		return null;
	}

	@Override
	public MasterStocks getMasterStocks(String operation) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RecordStockTrade getRecordStockTrade(String operation) {
		// TODO Auto-generated method stub
		return null;
	}

}
