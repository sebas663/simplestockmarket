package simple.stock.market.factories;

import simple.stock.market.services.MasterStocks;
import simple.stock.market.services.RecordStockTrade;
import simple.stock.market.services.StockIndicator;
import simple.stock.market.services.impl.MasterStocksImpl;

public class MasterStocksFactory extends AbstractFactory {

	@Override
	public StockIndicator getStockIndicator(String operation) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.factories.AbstractFactory#getMasterStocks(java.lang.String)
	 */
	@Override
	public MasterStocks getMasterStocks(String operation) {
		if (operation == null) {
			return null;
		}
		if (operation.equalsIgnoreCase("MasterStocksImpl")) {
			return new MasterStocksImpl();

		}
		return null;
	}

	@Override
	public RecordStockTrade getRecordStockTrade(String operation) {
		// TODO Auto-generated method stub
		return null;
	}

}
