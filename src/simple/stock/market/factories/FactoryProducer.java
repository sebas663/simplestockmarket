package simple.stock.market.factories;

public class FactoryProducer {
	
	/**
	 * 
	 * @param choice
	 * @return
	 */
	public static AbstractFactory getFactory(String choice) {
		if (choice.equalsIgnoreCase("StockIndicator")) {
			return new StockIndicatorFactory();
		}
		if (choice.equalsIgnoreCase("RecordStockTrade")) {
			return new RecordStockTradeFactory();
		}
		if (choice.equalsIgnoreCase("MasterStocks")) {
			return new MasterStocksFactory();
		}
		return null;
	}
}
