package simple.stock.market.factories;

import simple.stock.market.services.MasterStocks;
import simple.stock.market.services.RecordStockTrade;
import simple.stock.market.services.StockIndicator;
import simple.stock.market.services.impl.RecordStockTradeImpl;

public class RecordStockTradeFactory extends AbstractFactory {

	@Override
	public StockIndicator getStockIndicator(String operation) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MasterStocks getMasterStocks(String operation) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.factories.AbstractFactory#getRecordStockTrade(java.lang.String)
	 */
	@Override
	public RecordStockTrade getRecordStockTrade(String operation) {
		if (operation == null) {
			return null;
		}
		if (operation.equalsIgnoreCase("RecordStockTradeImpl")) {
			return new RecordStockTradeImpl();

		}
		return null;
	}

}
