package simple.stock.market.services;

import java.util.List;

import simple.stock.market.entities.Stock;
import simple.stock.market.entities.Trade;

public interface StockIndicator {

	/**
	 * Method that returns dividend yield from a stock.
	 * 
	 * @param price
	 * @param stock
	 * @return
	 */
	Double calculateDividendYield(Double price, Stock stock);

	/**
	 * 
	 * @param price
	 * @param stock
	 * @return
	 */
	Double calculatePERatio(Double price, Stock stock);

	/**
	 * 
	 * @param trades
	 * @return
	 */
	Double calculateVolumeWeightedStockPrice(List<Trade> trades);

	/**
	 * 
	 * @param volumeWeightedStockPrices
	 * @return
	 */
	Double calculateGBCEAllShareIndex(List<Double> volumeWeightedStockPrices);
}
