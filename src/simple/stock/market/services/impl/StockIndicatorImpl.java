package simple.stock.market.services.impl;

import java.util.List;

import simple.stock.market.entities.Stock;
import simple.stock.market.entities.Trade;
import simple.stock.market.services.Arithmetic;
import simple.stock.market.services.StockIndicator;

public class StockIndicatorImpl implements StockIndicator {
	
	private Arithmetic division = (x, y) -> x / y;
	private Arithmetic multiply = (x, y) -> x * y;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see simple.stock.market.StockOperation#calculateDividendYield(java.lang.Double, simple.stock.market.Stock)
	 */
	@Override
	public Double calculateDividendYield(Double price, Stock stock) {
		Double result = 0D;
		if (stock.getType().equals("Common")) {
			result = this.division.calculate(stock.getLastDivident(), price);
		} else {
			Double mul = this.multiply.calculate(stock.getFixedDivident(), stock.getParValue());
			result = this.division.calculate(mul, price);
		}
		return roundValue(result);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * simple.stock.market.StockOperation#calculatePERatio(java.lang.Double, simple.stock.market.Stock)
	 */
	@Override
	public Double calculatePERatio(Double price, Stock stock) {
		Double result = 0D;
		if (stock.getLastDivident() > 0) {
			result = roundValue(this.division.calculate(price, stock.getLastDivident()));
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * simple.stock.market.StockOperation#calculateVolumeWeightedStockPrice(java.util.List)
	 */
	@Override
	public Double calculateVolumeWeightedStockPrice(List<Trade> trades) {
		Double tradedPricexQuantity = trades.stream().mapToDouble(x -> x.getPrice() * x.getQuantity()).sum();
		Double quantity = trades.stream().mapToDouble(x -> x.getQuantity()).sum();
		return roundValue(this.division.calculate(tradedPricexQuantity, quantity));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * simple.stock.market.StockOperation#calculateGBCEAllShareIndex(java.util.List)
	 */
	@Override
	public Double calculateGBCEAllShareIndex(List<Double> volumeWeightedStockPrices) {
		Double multiply = volumeWeightedStockPrices.stream().reduce((a, b) -> a * b).get();
		Double pow = this.division.calculate(1D, (double) volumeWeightedStockPrices.size());
		Double value = Math.pow(multiply, pow);
		return roundValue(value);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	private Double roundValue(Double value) {
		Double round = 100d;
		Double r = (double) Math.round(this.multiply.calculate(value, round));
		return this.division.calculate(r, round);
	}

}
