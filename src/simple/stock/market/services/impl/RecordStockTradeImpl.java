package simple.stock.market.services.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import simple.stock.market.entities.Trade;
import simple.stock.market.services.RecordStockTrade;

public class RecordStockTradeImpl implements RecordStockTrade {
	
	/**
	 * 
	 */
	List<Trade> lTrades;

	/**
	 * Constructor.
	 */
	public RecordStockTradeImpl() {
		super();
		this.lTrades = new ArrayList<>();
	}

	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.RecordStockTrade#getTradesByStockSymbol(java.lang.String)
	 */
	public List<Trade> getTradesByStockSymbol(String stockSymbol) {
		return lTrades.stream().filter(x -> x.getStockSimbol().equals(stockSymbol)).collect(Collectors.toList());
	}

	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.RecordStockTrade#getLastFiveMinutesTradesByStockSymbol(java.lang.String)
	 */
	public List<Trade> getLastFiveMinutesTradesByStockSymbol(String stockSymbol) {
		LocalDateTime fiveMinutesAgo = LocalDateTime.now().minusSeconds(301);
		return lTrades.stream()
				.filter(x -> x.getStockSimbol().equals(stockSymbol) && x.getTimestamp().isAfter(fiveMinutesAgo))
				.collect(Collectors.toList());
	}

	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.RecordStockTrade#add(simple.stock.market.Trade)
	 */
	public boolean add(Trade e) {
		return lTrades.add(e);
	}

	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.RecordStockTrade#remove(simple.stock.market.Trade)
	 */
	public boolean remove(Trade e) {
		return lTrades.remove(e);
	}

	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.RecordStockTrade#getlTrades()
	 */
	public List<Trade> getlTrades() {
		return lTrades;
	}

}
