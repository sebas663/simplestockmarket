package simple.stock.market.services.impl;

import java.util.ArrayList;
import java.util.List;

import simple.stock.market.entities.Stock;
import simple.stock.market.services.MasterStocks;

public class MasterStocksImpl implements MasterStocks{
	/**
	 * 
	 */
	private List<Stock> lStoks;

	/**
	 * Constructor.
	 */
	public MasterStocksImpl() {
		super();
		this.lStoks = new ArrayList<Stock>();
	}

	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.MasterStocks#getStockBySymbol(java.lang.String)
	 */
	public Stock getStockBySymbol(String stockSymbol) {
		return lStoks.stream().filter(x -> x.getStockSymbol().equals(stockSymbol)).findAny().orElse(null);
	}

	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.MasterStocks#getlStoks()
	 */
	public List<Stock> getlStoks() {
		return lStoks;
	}

	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.MasterStocks#remove(simple.stock.market.Stock)
	 */
	public boolean remove(Stock e) {
		return lStoks.remove(e);
	}

	/*
	 * (non-Javadoc)
	 * @see simple.stock.market.MasterStocks#add(simple.stock.market.Stock)
	 */
	public boolean add(Stock e) {
		return lStoks.add(e);
	}
}
