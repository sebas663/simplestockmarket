package simple.stock.market.services;

@FunctionalInterface
public interface Arithmetic {
	Double calculate(Double a, Double b);
}
