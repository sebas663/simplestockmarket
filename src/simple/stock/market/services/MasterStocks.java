package simple.stock.market.services;

import java.util.List;

import simple.stock.market.entities.Stock;

public interface MasterStocks {
	
	/**
	 * 
	 * @param stockSymbol
	 * @return
	 */
	Stock getStockBySymbol(String stockSymbol);

	/**
	 * 
	 * @return
	 */
	List<Stock> getlStoks();

	/**
	 * 
	 */
	boolean remove(Stock e);

	/**
	 * 
	 * @param e
	 * @return
	 */
	boolean add(Stock e);
}
