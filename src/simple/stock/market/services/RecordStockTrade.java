package simple.stock.market.services;

import java.util.List;

import simple.stock.market.entities.Trade;

public interface RecordStockTrade {
	/**
	 * 
	 * @param stockSymbol
	 * @return
	 */
	public List<Trade> getTradesByStockSymbol(String stockSymbol);

	/**
	 * 
	 * @param stockSymbol
	 * @return
	 */
	public List<Trade> getLastFiveMinutesTradesByStockSymbol(String stockSymbol);

	/**
	 * 
	 * @param e
	 * @return
	 */
	public boolean add(Trade e);

	/**
	 * 
	 * @param e
	 * @return
	 */
	public boolean remove(Trade e);

	/**
	 * 
	 * @return
	 */
	public List<Trade> getlTrades();

}
