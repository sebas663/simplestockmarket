package simple.stock.market.entities;

import java.time.LocalDateTime;

public class Trade {

	private String stockSimbol;
	private LocalDateTime timestamp;
	private Integer quantity;
	private Boolean buyIndicator;
	private Boolean sellIndicator;
	private Double price;

	/**
	 * Constructor.
	 */
	public Trade() {
		super();
	}

	/**
	 * Constructor with fields.
	 */
	public Trade(String stockSimbol, LocalDateTime timestamp, Integer quantity, Boolean buyIndicator,
			Boolean sellIndicator, Double price) {
		super();
		this.stockSimbol = stockSimbol;
		this.timestamp = timestamp;
		this.quantity = quantity;
		this.buyIndicator = buyIndicator;
		this.sellIndicator = sellIndicator;
		this.price = price;
	}

	/**
	 * @return the stockSimbol
	 */
	public String getStockSimbol() {
		return stockSimbol;
	}

	/**
	 * @param stockSimbol
	 *            the stockSimbol to set
	 */
	public void setStockSimbol(String stockSimbol) {
		this.stockSimbol = stockSimbol;
	}

	/**
	 * @return the timestamp
	 */
	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the buyIndicator
	 */
	public Boolean getBuyIndicator() {
		return buyIndicator;
	}

	/**
	 * @param buyIndicator
	 *            the buyIndicator to set
	 */
	public void setBuyIndicator(Boolean buyIndicator) {
		this.buyIndicator = buyIndicator;
	}

	/**
	 * @return the sellIndicator
	 */
	public Boolean getSellIndicator() {
		return sellIndicator;
	}

	/**
	 * @param sellIndicator
	 *            the sellIndicator to set
	 */
	public void setSellIndicator(Boolean sellIndicator) {
		this.sellIndicator = sellIndicator;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return " [stockSimbol=" + stockSimbol + ", timestamp=" + timestamp + ", quantity=" + quantity + ", price="
				+ price + "]";
	}

}
