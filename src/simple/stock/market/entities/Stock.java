package simple.stock.market.entities;

public class Stock {

	private String stockSymbol;
	private String type;
	private Double lastDivident;
	private Double fixedDivident;
	private Double parValue;

	/**
	 * Constructor.
	 */
	public Stock() {
		super();
	}

	/**
	 * Constructor with fields.
	 */
	public Stock(String stockSymbol, String type, Double lastDivident, Double fixedDivident, Double parValue) {
		super();
		this.stockSymbol = stockSymbol;
		this.type = type;
		this.lastDivident = lastDivident;
		this.fixedDivident = fixedDivident;
		this.parValue = parValue;
	}

	/**
	 * @return the stockSymbol
	 */
	public String getStockSymbol() {
		return stockSymbol;
	}

	/**
	 * @param stockSymbol
	 *            the stockSymbol to set
	 */
	public void setStockSymbol(String stockSymbol) {
		this.stockSymbol = stockSymbol;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the lastDivident
	 */
	public Double getLastDivident() {
		return lastDivident;
	}

	/**
	 * @param lastDivident
	 *            the lastDivident to set
	 */
	public void setLastDivident(Double lastDivident) {
		this.lastDivident = lastDivident;
	}

	/**
	 * @return the fixedDivident
	 */
	public Double getFixedDivident() {
		return fixedDivident;
	}

	/**
	 * @param fixedDivident
	 *            the fixedDivident to set
	 */
	public void setFixedDivident(Double fixedDivident) {
		this.fixedDivident = fixedDivident;
	}

	/**
	 * @return the parValue
	 */
	public Double getParValue() {
		return parValue;
	}

	/**
	 * @param parValue
	 *            the parValue to set
	 */
	public void setParValue(Double parValue) {
		this.parValue = parValue;
	}

}
